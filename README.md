# Running tests with BuildBot

## Overview

This repository sets a Vagrantfile with additional scripts and configuration files
to run an example testing environment, based on Buildbot.

## Description

**You can just move to the "Hands on" section and ignore this bla-bla-bla here,
if you like.**

### How this approach implements the requirements

As the requirements set a couple of terms and constraints to follow, here we must
clarify how those terms and constraints match to the implementation.

BuildBot is an open source continuous integration system written in Python, based
on a free software license. It requires a setup with at least 2 components: 1
**master** and 1 **slave**.

The "master" sets a configuration, watching VCS (i.e. Git) repositories for
changes, scheduled tests or manually requested tests. It also provides a web
interface to manage the whole process.

The "slaves" can be many, each running in a different machine or **environment**,
with **limited hardware resources**, and it's responsible to run the tests. It
can also run in a Docker container.

As Buildbot orchestrates the slaves, running **one task at a time**, it's not
necessary to set any additional logic to do it.

### Requesting tests to run

As an interface to request tests, there are many ways:

- in **Builders** screen, the button "Force Build" allows manual test requests,
setting a specific repository, branch or tag (i.e. version)
- and/or Buildbot's "**schedulers**" can be setup automated test runs based on
updates, changes, time, etc.

Another option is to use Buildbot's JSON API to request that from command line:

http://localhost:8010/json/help

or here:

http://buildbot.buildbot.net/json/help

### Dashboard instead of updates via Ajax

That's not included in Buildbot, but I wrote a very simple Dashboard with Flask,
using server push notifications via Pusher (a push notification service).

Every build sends a few notifications to pusher, which updates the dashboard
screen live, showing if tests passed or failed, test coverage rate, including
nice intuitive colors.

The dashboard can be exposed in a TV screen for the whole team to watch it live.
It's a simple and not expensive at all, with good results for team's motivation.

### Glossary / Equivalences

- Test environment = Slave
- Requester = Buildbot user
- Request ID = hasn't an equivalent, but the timestamp can be used
- Templates = Buildbot's **Builders**, based on a branch
- Choose files to test = can be set by Buildbot's **filters** and a more flexible
Makefile
- List of previous test runs = each builder has it's own history list

### Related repositories

#### vagrant machine

https://bitbucket.org/testpb/vagrant-buildbot

#### example test project

https://bitbucket.org/testpb/project1/

That's written in Python as a simple package, containing 2 modules with a class
each. Source code and Makefile are self-explanatory.

## Hands on!

### 1. Dependencies

All's needed to run this vagrant machine is:

- VirtualBox
- Vagrant
- Git

You also must certify that you got stable internet connection (around 1~1.5Gb
will be downloaded) and ports 8010 and 9020 in host machine is free.

### 2. Setup

    # Clone this repository in a folder:
    git clone https://marinho@bitbucket.org/testpb/vagrant-buildbot.git
    cd vagrant-buildbot

    # Bootup the machine
    vagrant up

### 3. Watching the Dashboard

For a nicer view of the running tests, as a first step, open the following
address in your browser (it must support WebSockets, so, Chrome is the most
recommended):

http://localhost:9020

As the tests below are ran, keep the eyes on the Dashboard for live updates. Try
it in your smartphone too. It's not necessary to refresh.

### 4. First test run - full

1. Open http://localhost:8010/ the browser
2. Login (top/right corner)
2.1. username: admin
2.2. password: 1
3. Click on "Builders" at top menu
4. Go to "Force All Builds" section
5. Click on "Force Build"
6. Click on "Waterfall" at top menu
6.1. Left column (project1_errors) must fail in "make test" stage - red
6.2. Right column (project1_successful) must pass - green

### 5. Testing manually another branch

1. Open the browser, login as above and go to "Builders"
2. Click on "project1_successful" in "Builders:" section
3. Go to "Force build" section
4. Type "Testing other-branch" in "reason"
5. Type "other-branch" in "Branch"
6. Click on "Force Build"
7. Refresh the page or click on "Show more" (above "Buildslaves" section)
8. The top item is the requested build/test
9. Click on the link in "Build #" column

## How test project work

### Branch "master"

Contains version as meant to be ready for production.

### Branch "staging"

Contains version with tests passing, as they must be in staging machine for Q&A
validation as the last step before going to production.

### Branch "task-with-errors"

It's expected as a "standard" branch in Buildbot, but it is not passing:
  https://bitbucket.org/testpb/project1/src/38b05dce34cf535362614c212b0e570d247598ce/project1/extras.py?at=master#cl-8

### Branch "other-branch"

This is another branch, not expected by Buildbot, to simulate a single branch
any developer can create with their implementation for a manual test request.

Tests aren't passing, as you can see here:
  https://bitbucket.org/testpb/project1/src/f5ee0edf278775d1faea148c9d719d87e170d532/tests/second.py?at=master#cl-16

## Relevant paths in the virtual machine

Use "vagrant ssh" to get access to it.

### /etc/supervisor/conf.d/

Contains supervisor configuration files, with running services (master and slave)

### /home/vagrant/master

Contains the Buildbot's master service

### /home/vagrant/slave

Contains the Buildbot's "example-slave" service
