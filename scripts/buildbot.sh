#/bin/bash

# This script must be executed as "vagrant"

cp /vagrant/files/bash_profile /home/vagrant/.bash_profile

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# Buildbot master

echo
echo Preparing Buildbot master
if [ ! -e "./master" ];
then
  buildbot create-master ./master
fi

# /vagrant is mapped to the host machine's "vagrant-buildbot" folder
cp /vagrant/files/master.cfg ./master/

# Buildbot slave

echo
echo Preparing Buildbot slave
if [ ! -e "./slave" ];
then
  buildslave create-slave slave localhost:9989 example-slave pass
fi
