#/bin/bash

# This script must be executed as "root"

apt-get update -y
apt-get upgrade -y
locale-gen en_GB.UTF-8

echo
echo Basic packages
apt-get install -y build-essential curl dkms linux-headers-server\
                   openssh-server htop aptitude nfs-kernel-server

echo
echo Python packages
apt-get install -y python-dev python-virtualenv python-pip python-flup \
                   libxml2-dev libxslt1-dev python-dev lib32z1-dev


echo
echo Buildbot
pip install buildbot
pip install buildbot-slave

echo
echo Flask
pip install flask
