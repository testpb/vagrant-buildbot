#/bin/bash

# This script must be executed as "root"

# Install supervisor package
apt-get install -y supervisor

# Setup supervisor

cp /vagrant/files/supervisor-master.conf /etc/supervisor/conf.d/master.conf
cp /vagrant/files/supervisor-slave.conf /etc/supervisor/conf.d/slave.conf
cp /vagrant/files/supervisor-dashboard.conf /etc/supervisor/conf.d/dashboard.conf

supervisorctl reread
supervisorctl update
supervisorctl restart all
