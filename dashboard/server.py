from flask import Flask
app = Flask(__name__)


@app.route("/")
def index():
    """As this is a too simple static file and nothing else, it's not necessary
    to work with template rendering in the officially proper way."""
    fp = file('index.html')
    content = fp.read()
    fp.close()

    return content


if __name__ == "__main__":
    app.run(host='0.0.0.0')
