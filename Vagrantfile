# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # Runs on Debian Wheezy
  config.vm.box = "nfq/wheezy"

  # Allocates "http://localhost:8010" to the VM's Buildbot interface
  config.vm.network "forwarded_port", guest: 8010, host: 8010
  config.vm.network "forwarded_port", guest: 5000, host: 9020

  # Allocates 1Gb RAM memory for the VM
  config.vm.provider "virtualbox" do |vb|
    vb.customize ["modifyvm", :id, "--memory", "1024"]
  end

  # Shell script to install necessary Debian packages
  config.vm.provision "shell", path: "scripts/packages.sh"

  # Shell script to setup Buildbot environment
  config.vm.provision "shell", path: "scripts/buildbot.sh", privileged: false

  # Shell script to setup the Dashboard server
  config.vm.provision "shell", path: "scripts/dashboard.sh", privileged: false

  # Shell script to setup supervisor with master and slave running
  config.vm.provision "shell", path: "scripts/supervisor.sh"
end
